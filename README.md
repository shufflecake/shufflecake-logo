# shufflecake-logo

Logo for [Shufflecake](https://www.shufflecake.net).


## Copyright

Copyright The Shufflecake Project Logo Authors (2022)

Copyright The Shufflecake Project Logo Contributors (2022)

Copyright Contributors to the The Shufflecake Project.

See the AUTHORS file at the top-level directory of this distribution and at <https://www.shufflecake.net/permalinks/shufflecake-logo/AUTHORS>.

This work is released under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). See the COPYRIGHT file at the top-level directory of this distribution and at <https://www.shufflecake.net/permalinks/shufflecake-logo/COPYRIGHT>.


